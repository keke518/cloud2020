package com.keboom.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author keboom
 * @date 2021/5/12 19:28
 */
@SpringBootApplication
public class StreamMQMain8803 {
    public static void main(String[] args)
    {
        SpringApplication.run(StreamMQMain8803.class,args);
    }
}
