package com.keboom.springcloud.service.impl;

import com.keboom.springcloud.dao.PaymentDao;
import com.keboom.springcloud.entities.Payment;
import com.keboom.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author keboom
 * @date 2021/5/5 13:30
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
