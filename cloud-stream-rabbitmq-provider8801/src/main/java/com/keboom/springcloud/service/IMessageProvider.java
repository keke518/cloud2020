package com.keboom.springcloud.service;

/**
 * @author keboom
 * @date 2021/5/11 20:19
 */
public interface IMessageProvider {
    public String send();
}
